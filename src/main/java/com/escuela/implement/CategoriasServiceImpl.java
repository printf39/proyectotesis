package com.escuela.implement;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.escuela.model.Categoria;
import com.escuela.service.ICategoriasService;

@Service
public class CategoriasServiceImpl implements ICategoriasService {
	
	private List<Categoria> listaCategorias = null;
	
	//Crea una lista de Categorias
	public CategoriasServiceImpl() {
		
		listaCategorias = new LinkedList<Categoria>();
		
		try {
			
			//Se Crea una categoria
			Categoria categoria1 = new Categoria();
			categoria1.setId(1);
			categoria1.setNombre("Computo");
			categoria1.setDescripcion("Area de Tecnologia");
			
			//Se Crea una categoria
			Categoria categoria2 = new Categoria();
			categoria2.setId(2);
			categoria2.setNombre("Contaduria");
			categoria2.setDescripcion("Manejo de Finanzas");
			
			listaCategorias.add(categoria1);
			listaCategorias.add(categoria2);
			
			
		} catch (Exception e) {
			System.err.println("No se pudo crear la cateroria: "+ e.getMessage());
		}
		
	}

//	------------------------------------------------------------
	public List<Categoria> buscarCategorias() {
		System.out.println(listaCategorias);
		return listaCategorias;
	}
	
//	------------------------------------------------------------
	//Guarda una nueva Categoria
	public void guardar(Categoria categoria) {
		listaCategorias.add(categoria);
	}

//	------------------------------------------------------------
	//Busca por idCategoria
	public Categoria buscarPorId(Integer idCategoria) {
		
		for(Categoria categoria: listaCategorias ) {
			if(categoria.getId() == idCategoria) {
				return categoria;
			}
			
		}
		
		return null;
	}

	@Override
	public void elimianarCategoria(Integer idCategoria) {
		// TODO Auto-generated method stub
		
	}
	
}
