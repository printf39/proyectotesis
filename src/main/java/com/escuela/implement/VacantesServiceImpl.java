package com.escuela.implement;

import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.escuela.model.Vacante;
import com.escuela.service.IVacantesService;

@Service
public class VacantesServiceImpl implements IVacantesService {
	
	private List<Vacante> listaVacantes = null; 
	
	//Metodo Constructor que implemneta la lista de vacantes
	public VacantesServiceImpl() {
		
		SimpleDateFormat parsearFecha = new SimpleDateFormat("dd-MM-yyyy");
		listaVacantes = new LinkedList<Vacante>();
		
		try {
			//Se crea un vacante nuevo
			Vacante vacante = new Vacante();
			vacante.setId(0);
			vacante.setNombre("jose");
			vacante.setDescripcion("Desarrollador Java");
			vacante.setFecha(parsearFecha.parse("12-10-1997"));
			vacante.setSalario(5000.0);
			vacante.setDestacado(0);
			vacante.setImagen("c1.png");
			
			//Se crea un vacante nuevo
			Vacante vacante1 = new Vacante();
			vacante1.setId(1);
			vacante1.setNombre("carlos");
			vacante1.setDescripcion("doctor");
			vacante1.setFecha(parsearFecha.parse("12-10-1997"));
			vacante1.setSalario(5000.0);
			vacante1.setDestacado(1);
			vacante1.setImagen("c2.png");
			
			//Se crea un vacante nuevo
			Vacante vacante2 = new Vacante();
			vacante2.setId(2);
			vacante2.setNombre("araceli");
			vacante2.setDescripcion("enfermera");
			vacante2.setFecha(parsearFecha.parse("12-10-1997"));
			vacante2.setSalario(5000.0);
			vacante2.setDestacado(1);
			vacante2.setImagen("c2.png");
			
			//Se crea un vacante nuevo
			Vacante vacante3 = new Vacante();
			vacante3.setId(3);
			vacante3.setNombre("fernando");
			vacante3.setDescripcion("ingeniero");
			vacante3.setFecha(parsearFecha.parse("12-10-1997"));
			vacante3.setSalario(5000.0);
			vacante3.setDestacado(0);
			
			//Se crea un vacante nuevo
			Vacante vacante4 = new Vacante();
			vacante4.setId(4);
			vacante4.setNombre("ana");
			vacante4.setDescripcion("abogada");
			vacante4.setFecha(parsearFecha.parse("12-10-1997"));
			vacante4.setSalario(5000.0);
			vacante4.setDestacado(1);
			
			//Agregar las vacantes a la lista 
			listaVacantes.add(vacante);
			listaVacantes.add(vacante1);
			listaVacantes.add(vacante2);
			listaVacantes.add(vacante3);
			listaVacantes.add(vacante4);
			
		} catch (Exception e) {
			System.err.println("error en vacantes" + e.toString());
		}
		
	}
	
	//Metodo que regresa la lista de vacantes
	public List<Vacante> buscarTodoALV() {
		return listaVacantes;
	}
	
	
	public Vacante buscarPorId(Integer idVacante) {
		
		for(Vacante v: listaVacantes) {
			if(v.getId() == idVacante) {
				return v;
			}
		}
		return null;
	}
	
	public void guardar(Vacante vacante) {
		listaVacantes.add(vacante);
	}

	@Override
	public List<Vacante> buscarDestacadas() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eliminarVacantes(Integer idVacante) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Vacante> buscarByExample(Example<Vacante> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Vacante> buscarTodas(Pageable page) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
