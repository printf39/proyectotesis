package com.escuela.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.escuela.model.Categoria;
import com.escuela.service.ICategoriasService;

@Controller
@RequestMapping(value="/categorias")
public class CategoriaController {
	
//	Inyeccion de dependencias ----------------------------------
	@Autowired
	private ICategoriasService serviceCategoria;
//	------------------------------------------------------------	
	
	@RequestMapping(value="/index", method=RequestMethod.GET)
	public String mostrarIndex(Model model) {
		
		List<Categoria> recuperaCategorias = serviceCategoria.buscarCategorias();
		
		model.addAttribute("categorias", recuperaCategorias);
		
		System.out.println(recuperaCategorias);
		
		return "categorias/listCategorias";
	}
	
//	------------------------------------------------------------
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String crear(Categoria categoria) {
		
		//	Se retorna la vista que llena el formulario
		return "categorias/formCategoria";
	}
	
//	------------------------------------------------------------
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public String guardar(Categoria categoria, BindingResult errorResults, RedirectAttributes redirecciona) {
		
		String msg = "El registro se guardo de forma Exitosa"; 
		
		if(errorResults.hasErrors()) {
			for(ObjectError error: errorResults.getAllErrors()) {
				System.out.println("El error se presento en: "+ error);
			}
			return "categorias/formCategoria";
		}
		
		//Despues del formulario si todo es correcto se guarda la Categoria
		serviceCategoria.guardar(categoria);
		
		System.out.println("Objeto Categoria: " + categoria);
		redirecciona.addFlashAttribute("message", msg);
		
		return "redirect:/categorias/index";
	}
	
	@RequestMapping(value="/delete/{id}",method=RequestMethod.GET)
	public String eliminarCategoria(@PathVariable("id") int id, Model model,RedirectAttributes redirecciona) {
		
		System.out.println("borrando vacante con id: "+id);
		
		this.serviceCategoria.elimianarCategoria(id);;
		
		redirecciona.addFlashAttribute("message","la Categoria fue eliminada");
		
		return "redirect:/categorias/index";
	}
	
	@RequestMapping(value="/editar/{id}", method=RequestMethod.GET)
	public String guardar(@PathVariable("id") Integer id, Model model) {
		Categoria categoria = this.serviceCategoria.buscarPorId(id);
		model.addAttribute("categoria", categoria);
		return "categorias/formCategoria";
	}
	
}

	