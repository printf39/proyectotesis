package com.escuela.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.escuela.model.Categoria;
import com.escuela.model.Vacante;
import com.escuela.service.ICategoriasService;
import com.escuela.service.IVacantesService;
import com.escuela.util.Utileria;

@Controller
@RequestMapping(value="/vacantes")
public class VacantesControlles {
	
	@Value("${pruebademo.ruta.imagenes}")
	String ruta;
	
//	Inyeccion de dependencias ----------------------------------	
	@Autowired 
	private IVacantesService serviceVacantes;
	
	@Autowired
	private ICategoriasService serviceCategorias;
//	------------------------------------------------------------
	
	//Recuperar lista de los vacantes
	@GetMapping("/index")
	public String mostrarIndex(Model model) {
		
		List<Vacante> recuperaVacantes = serviceVacantes.buscarTodoALV();
		
		model.addAttribute("recuperaVacantes", recuperaVacantes);
		System.out.println(recuperaVacantes);
		return "vacantes/listVacantes";
	}
	
//------------------------------------------------------------
	@RequestMapping(value="/indexPaginate",method= RequestMethod.GET)
	public String mostrarPaginado(Model model, Pageable page) {
		Page<Vacante> lista = serviceVacantes.buscarTodas(page);
		model.addAttribute("recuperaVacantes", lista);
		return "vacantes/listVacantes";
	}		
//------------------------------------------------------------
	
	//Regresa la vista del formulario para vacantes
	@RequestMapping(value="/create", method=RequestMethod.GET)
	public String crear(Vacante vacante, Model model) {
		
		List<Categoria> listaCategorias = serviceCategorias.buscarCategorias();
		
		model.addAttribute("categorias", listaCategorias);
		
		return "vacantes/formVacante";
	}
	
//	------------------------------------------------------------	
	//Consume la vista del formulario para vacantes
	@PostMapping("/save") 
	public String guardar(Vacante vacante, BindingResult errorResult, RedirectAttributes redirecA,
			@RequestParam("archivoImagen") MultipartFile multiPart) {
		
		String msg="Registro Guardado Exitosamente";
		
		if(errorResult.hasErrors()) {
			
			for(ObjectError error: errorResult.getAllErrors()) {
				System.out.println("ocurrio un error en: "+ error.getDefaultMessage());
			}
			return "vacantes/formVacante";
		}
		
		
		//Validacion de la imagen metodo que recibe el archivo y la ruta
		if(!multiPart.isEmpty()) {
			
//			String ruta = "c:/empleos/img-vacantes/";
			//Se llama a un metodo estatico
			String nombreImagen = Utileria.guardarArchivo(multiPart, ruta);
			
			//Si la imagen no viene nula 
			if(nombreImagen != null) {
				//Se procesa la variable nombreImagen
				vacante.setImagen(nombreImagen);
			}
		}
		serviceVacantes.guardar(vacante);
		
		System.out.println("Objeto Vacante: " + vacante);
		redirecA.addFlashAttribute("message", msg);
		
		return "redirect:/vacantes/index";
	}
	
//	------------------------------------------------------------	
	//Data binding para tipos Date
	@InitBinder
	public void initbinder(WebDataBinder webDataBinder) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
		
	}
		
//	------------------------------------------------------------
	@RequestMapping(value="/delete/{id}",method=RequestMethod.GET)
	public String eliminar(@PathVariable("id") int id, Model model,RedirectAttributes redirecciona) {
		
		System.out.println("borrando vacante con id: "+id);
		
		this.serviceVacantes.eliminarVacantes(id);
		
//		model.addAttribute("idDelete", id);
		redirecciona.addFlashAttribute("message","la vacante fue eliminada");
		
		return "redirect:/vacantes/index";
	}
	
//	------------------------------------------------------------
	//editar una vacante
	@GetMapping("/editar/{id}")
	public String editar(@PathVariable("id") Integer id,Model model) {
		Vacante vac = serviceVacantes.buscarPorId(id);
		List<Categoria> listaCategorias = serviceCategorias.buscarCategorias();

		model.addAttribute("vacante", vac);
		model.addAttribute("categorias", listaCategorias);
		
		return "vacantes/formVacante";
	}
	
//	------------------------------------------------------------
	
	@RequestMapping(value="/view/{id}",method=RequestMethod.GET)
	public String verDetalles(@PathVariable("id") int idVacante, Model model) {
		
		Vacante vacante = serviceVacantes.buscarPorId(idVacante);
		
		System.out.println("vacante: "+"idVacante ["+idVacante+"] " + vacante);
		
		model.addAttribute("listaVacante", vacante);
		
		return "detalle";
	}
	
}
