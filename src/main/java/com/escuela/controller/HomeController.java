package com.escuela.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.escuela.model.Usuario;
import com.escuela.model.Vacante;
import com.escuela.service.ICategoriasService;
import com.escuela.service.IUsuariosService;
import com.escuela.service.IVacantesService;

@Controller
public class HomeController {
	
	@Autowired
	private IVacantesService vacantesService;
	
	@Autowired
	private IUsuariosService usuariosService;
	
	@Autowired
	private ICategoriasService servicioCategoria;
	
//	------------------------------------------------------------
	@RequestMapping(path="/tablas",method=RequestMethod.GET)
	public String mostrarTabla(Model model) {
		
//		List<Vacante> lista = getVacantes();
		List<Vacante> lista = vacantesService.buscarTodoALV();
		
		model.addAttribute("despliegaListaVacantes", lista);
		
		return "tabla";
	}
	
//	------------------------------------------------------------	
	@RequestMapping(path="/detalle", method=RequestMethod.GET)
	public String mostrarDetalle(Model model) {
		
		Vacante vacante = new Vacante();
		
		vacante.setNombre("Jose Luis Torres Coronel");
		vacante.setDescripcion("Desarrollador Back-End");
		vacante.setFecha(new Date());
		vacante.setSalario(9700.00);
		
		model.addAttribute("listaVacante", vacante);
		
		return "detalle";
	}
	
//---------------------------------------------------------------------------------
	//1------------------------------------------------------
	@RequestMapping(path="/listado", method=RequestMethod.GET)
	public String muestraPagina(Model model) {
		
//			model.addAttribute("categorias", this.servicioCategoria.buscarCategorias());
		
		return "home";
	}
	
	//Metodo de Models
	@ModelAttribute
	public void setGenericos(Model model) {
		
		List<Vacante> lista = vacantesService.buscarDestacadas();
		System.out.println(lista.size());
		
		model.addAttribute("vacante", lista);
		
		Vacante vacanteSerch = new Vacante();
		vacanteSerch.cambiaValorImagen();
		model.addAttribute("buscaVacante", vacanteSerch);
		
		model.addAttribute("categorias", this.servicioCategoria.buscarCategorias());
	}
	
	
	@GetMapping("/buscaVacante")
	public String buscar(@ModelAttribute("buscaVacante") Vacante vacante, Model model) {
		System.out.println(vacante);
		
		//Consulta select con like '%?%
		ExampleMatcher martcher = ExampleMatcher.
				matching().withMatcher("descripcion", ExampleMatcher.GenericPropertyMatchers.contains());
		
		Example<Vacante> vacanteExample = Example.of(vacante,martcher);
		List<Vacante> listadoExampleVacantes = vacantesService.buscarByExample(vacanteExample);
		model.addAttribute("vacante", listadoExampleVacantes);
		
		return "home";
	}
	
	//InitBindir para Strings  si los detecta vacios en el Data Binding los settea a Null
	@InitBinder
	public void initBindir(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	//1------------------------------------------------------------
//---------------------------------------------------------------------------------
	
	
	private List<Vacante> getVacantes() {
	
		SimpleDateFormat parsearFecha = new SimpleDateFormat("dd-MM-yyyy");
		List<Vacante> listaVacantes = new LinkedList<Vacante>();
		
		
		try {
			//Se crea un vacante nuevo
			Vacante vacante = new Vacante();
			vacante.setId(0);
			vacante.setNombre("jose");
			vacante.setDescripcion("empleado");
			vacante.setFecha(parsearFecha.parse("12-10-1997"));
			vacante.setSalario(5000.0);
			vacante.setDestacado(0);
			vacante.setImagen("c1.png");
			
			//Se crea un vacante nuevo
			Vacante vacante1 = new Vacante();
			vacante1.setId(1);
			vacante1.setNombre("carlos");
			vacante1.setDescripcion("doctor");
			vacante1.setFecha(parsearFecha.parse("12-10-1997"));
			vacante1.setSalario(5000.0);
			vacante1.setDestacado(1);
			vacante1.setImagen("c2.png");
			
			//Se crea un vacante nuevo
			Vacante vacante2 = new Vacante();
			vacante2.setId(2);
			vacante2.setNombre("araceli");
			vacante2.setDescripcion("enfermera");
			vacante2.setFecha(parsearFecha.parse("12-10-1997"));
			vacante2.setSalario(5000.0);
			vacante2.setDestacado(1);
			vacante2.setImagen("c2.png");
			
			//Se crea un vacante nuevo
			Vacante vacante3 = new Vacante();
			vacante3.setId(3);
			vacante3.setNombre("fernando");
			vacante3.setDescripcion("ingeniero");
			vacante3.setFecha(parsearFecha.parse("12-10-1997"));
			vacante3.setSalario(5000.0);
			vacante3.setDestacado(0);
			
			//Se crea un vacante nuevo
			Vacante vacante4 = new Vacante();
			vacante4.setId(4);
			vacante4.setNombre("ana");
			vacante4.setDescripcion("abogada");
			vacante4.setFecha(parsearFecha.parse("12-10-1997"));
			vacante4.setSalario(5000.0);
			vacante4.setDestacado(1);
			
			//Agregar las vacantes a la lista 
			listaVacantes.add(vacante);
			listaVacantes.add(vacante1);
			listaVacantes.add(vacante2);
			listaVacantes.add(vacante3);
			listaVacantes.add(vacante4);
			
		} catch (Exception e) {
			System.err.println("error en vacantes" + e.toString());
		}
		
		return listaVacantes;
	}
	
	//-------------------------------------------------------------------
	//Usuarios
	
	//Mostrar el formulario
	@GetMapping("/registro/usuario")
	public String registrarse(Usuario usuario) {
		
		return "registro/formRegistro";
	}
	
	//Realizar el registro
	@PostMapping("/registro/usuario")
	public String guardarRegistro(Usuario usuario, BindingResult errores,RedirectAttributes redirecciona) {
		String msg = "El registro se guardo de forma Exitosa";
		if(errores.hasErrors()) {
			for(ObjectError error: errores.getAllErrors()) {
				System.out.println("El error se presento en: "+ error);
			}
			return "registro/formRegistro";
		}
		usuariosService.guardar(usuario);
		System.out.println("Objeto Usuario: " + usuario);
		redirecciona.addFlashAttribute("message", msg);
		return "redirect:/usuarios/index";
	}

}
