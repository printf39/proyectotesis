package com.escuela.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.escuela.model.Usuario;
import com.escuela.service.IUsuariosService;

@Controller
@RequestMapping("/usuarios")
public class UsuariosController {
	
		@Autowired
		private IUsuariosService servicioUser;

		@GetMapping("/index")
		public String mostrarIndex(Model model) {
			List<Usuario> usuarios = servicioUser.buscarTodos();
			model.addAttribute("usuarios", usuarios);
			return "registro/listUsuarios";
		}
		
		@GetMapping("/delete/{id}")
		public String eliminar(@PathVariable("id") Integer id, RedirectAttributes modelAtributos) {
			String msg="El usuario fue eliminado de forma Correcta";
			servicioUser.eliminar(id);
			modelAtributos.addFlashAttribute("message", msg);
			return "redirect:/usuarios/index";
		}
}
