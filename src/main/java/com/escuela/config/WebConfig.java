package com.escuela.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
	
	public void addResourceHandlers(ResourceHandlerRegistration registro) {
		registro.addResourceLocations("/logos").addResourceLocations("file:C:/empleos/img-vacantes/");
	}
	
}
