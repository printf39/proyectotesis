package com.escuela.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.escuela.model.Categoria;

@Repository
public interface CategoriasRepository extends JpaRepository<Categoria, Integer>{

}
