package com.escuela.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.escuela.model.Vacante;

@Repository
public interface VacantesRepository extends JpaRepository<Vacante, Integer> {
	
	//Aqui de declaran los Query methods
	
	//Regresa Lista de Estatus
	List<Vacante> findByEstatus(String estatus);
	
	//Regresa Lista de Estatus and Destacado 
	List<Vacante> findByDestacadoAndEstatus(Integer destacado, String estatus);
	
	//Regresa Lista de Estatus and Destacado y ordenado de forma Descendente
	List<Vacante> findByDestacadoAndEstatusOrderByIdDesc(Integer destacado, String estatus);
	
	//Buscar vacantes utilizando un Rango de Salarios
	List<Vacante> findBySalarioBetween(double sMenor, double sMayor);
	
	//Buscar Una Lista Estatus
	List<Vacante> findByEstatusIn(String[] multiplesEstatus);
	
	
}
