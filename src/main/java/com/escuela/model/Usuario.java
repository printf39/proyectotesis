package com.escuela.model;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "usuarios")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "username")
	private String nombreUsuario;
	
	@Column(name = "password")
	private String contrasena;
	
	@Column(name = "estatus")
	private Integer estatus;
	
	@Column(name = "fecharegistro")
	private Date fechaRegistro;
	
	//Configuracion muchos a muchos 
	@ManyToMany(fetch = FetchType.EAGER) //Esta configuracion especifica que cuando hace una consulta recupera el listado total
	@JoinTable( name= "usuarioperfil",
	            joinColumns = @JoinColumn(name = "idusuario"),
			    inverseJoinColumns = @JoinColumn(name = "idperfil")
			)
	private List<Perfil> perfiles;
	
	//Metodo de ayuda para guargar perfiles;
	public void agregaPerfiles(Perfil perfil){
		
		if(this.perfiles == null) {
			this.perfiles  = new LinkedList<Perfil>();
		}
		
		this.perfiles.add(perfil);
	}
	 
	
	public List<Perfil> getPerfiles() {
		return perfiles;
	}

	public void setPerfiles(List<Perfil> perfiles) {
		this.perfiles = perfiles;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nombre=" + nombre + ", email=" + email + ", nombreUsuario=" + nombreUsuario
				+ ", contrasena=" + contrasena + ", estatus=" + estatus + ", fechaRegistro=" + fechaRegistro
				+ ", perfiles=" + "" + "]";
	}

}
