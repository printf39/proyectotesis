package com.escuela.util;

import java.io.File;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;


public class Utileria {
	
	//Metodo que Guarda el archivo
	public static String guardarArchivo(MultipartFile multiPArt ,String ruta) {
		
		//Obtener el nombre del archivo a procesar
		String nombreAleatorio = Utileria.randomAlphaNumeric(5);
		
		String nombreOriginal = multiPArt.getOriginalFilename();
		nombreOriginal = nombreOriginal.replace(" ", "-");
		
		nombreOriginal =  nombreAleatorio + "_" + nombreOriginal;
		
		try {
			
			//Formar el nombre del archivo para guardarlo en el disco Duro
			File imageFile = new File(ruta + nombreOriginal);
			System.out.println("Archivo: " + imageFile.getAbsoluteFile());
			
			multiPArt.transferTo(imageFile);
			
			return nombreOriginal;
			
		} catch (IOException ioE) {
			System.out.println("erro " + ioE.getMessage());
			return null;
		}
	}
	
	/*
	 * Metodo para generar una cadena Aleatoria de longitud N
	 * @param count
	 * @return 
	 */
	public static String randomAlphaNumeric(int count) {
		
		String CARACTERES = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder builder = new StringBuilder();
		
		while (count-- != 0) {
			int character = (int) (Math.random() * CARACTERES.length());
			
			builder.append(CARACTERES.charAt(character));
		}
		return builder.toString();
	}
	
}
