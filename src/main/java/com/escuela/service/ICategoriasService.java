package com.escuela.service;

import java.util.List;

import com.escuela.model.Categoria;

public interface ICategoriasService {
	
	List<Categoria> buscarCategorias();
	
	void guardar(Categoria categoria);
	
	Categoria buscarPorId(Integer idCategoria);
	
	void elimianarCategoria(Integer idCategoria);
	
}
