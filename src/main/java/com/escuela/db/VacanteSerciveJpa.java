package com.escuela.db;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.escuela.model.Vacante;
import com.escuela.repository.VacantesRepository;
import com.escuela.service.IVacantesService;

@Service
@Primary
public class VacanteSerciveJpa implements IVacantesService{

	@Autowired
	private VacantesRepository repositoVacantes;
	
	@Override
	public List<Vacante> buscarTodoALV() {
		return this.repositoVacantes.findAll();
	}

	@Override
	public Vacante buscarPorId(Integer idVacante) {
		Optional<Vacante> vacantes = repositoVacantes.findById(idVacante);
		
		if (vacantes.isPresent()) {
			return vacantes.get();
		} else {
			return null;
		}
	}

	@Override
	public void guardar(Vacante vacante) {
		this.repositoVacantes.save(vacante);
	}

	@Override
	public List<Vacante> buscarDestacadas() {
		return this.repositoVacantes.findByDestacadoAndEstatusOrderByIdDesc(1, "Aprobada");
	}

	@Override
	public void eliminarVacantes(Integer idVacante) {
		this.repositoVacantes.deleteById(idVacante);
	}

	//busqueda por Query By Example
	@Override
	public List<Vacante> buscarByExample(Example<Vacante> example) {
		return this.repositoVacantes.findAll(example);
	}

	@Override
	public Page<Vacante> buscarTodas(Pageable page) {
		return this.repositoVacantes.findAll(page);
	}
}
