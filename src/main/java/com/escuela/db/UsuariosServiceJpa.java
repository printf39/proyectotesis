package com.escuela.db;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.escuela.model.Perfil;
import com.escuela.model.Usuario;
import com.escuela.repository.UsuarioRepository;
import com.escuela.service.IUsuariosService;

@Service
public class UsuariosServiceJpa implements IUsuariosService{
	
	//Inyeccion de Dependencias de Repositorio
	@Autowired
	private UsuarioRepository repositorioUsuario;

	@Override
	public void guardar(Usuario usuario) {
		
		Date fechaHoy = new Date();
		
		List<Perfil> perfilAsignar = new LinkedList<Perfil>();
		
		Perfil perfilUsuario = new Perfil();
		perfilUsuario.setId(3);
		
		perfilAsignar.add(perfilUsuario);
		usuario.setEstatus(1);
		usuario.setPerfiles(perfilAsignar);
		usuario.setFechaRegistro(fechaHoy);
		
		this.repositorioUsuario.save(usuario);
	}

	@Override
	public void eliminar(Integer idUsuario) {
		this.repositorioUsuario.deleteById(idUsuario);
	}

	@Override
	public List<Usuario> buscarTodos() {
		return this.repositorioUsuario.findAll();
	}

}
