package com.escuela.db;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.escuela.model.Categoria;
import com.escuela.repository.CategoriasRepository;
import com.escuela.service.ICategoriasService;

@Primary
@Service
public class CategoriaServiceJpa implements ICategoriasService{
	
	@Autowired
	CategoriasRepository repositorioCategorias;

	@Override
	public List<Categoria> buscarCategorias() {
		List<Categoria> listaCategorias = repositorioCategorias.findAll();
		return listaCategorias;
	}

	@Override
	public void guardar(Categoria categoria) {
		repositorioCategorias.save(categoria);
	}

	@Override
	public Categoria buscarPorId(Integer idCategoria) {
		
		Optional<Categoria> regresaId = repositorioCategorias.findById(idCategoria);
		
		if (regresaId.isPresent()) {
			return regresaId.get();
		} else {
			return null;
		}
	}

	@Override
	public void elimianarCategoria(Integer idCategoria) {
		this.repositorioCategorias.deleteById(idCategoria);
	}
	

}
